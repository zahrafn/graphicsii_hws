Zahra Forootaninia 4464235

HW2: Here I the PBRT codes that I modified: 

api.cpp :
	At the end of the makeShape function I added 
	lines of code that pass the new shape if it exist. 
	It can only exist if it's for triangle shape.

shape.h

	Added virtual optimization method


trianglemesh.cpp

	added 4 functions:

		- EachTriangle -> that is a struct which
				contains a triangle properties

		- split -> this function gets one triangle
				 and split it into two triangles

		-earlySplit -> this is a recursive function 
				that check if triangles need to 
				be divided if so it start dividing 
				recursively otherwise in the base case
				 triangle added to an array of triangles.


		- Optimize -> in this method we go through out old mesh
				 and get triangles and run a ealySplit
				method on it. then we use new triangle array
				to make new mesh and fill out all the 
				properties of the triangles 
