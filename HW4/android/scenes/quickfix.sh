#!/bin/sh

# tonemap settings and halt time
for v in *.lxs
do
    sed -E 's|"string tonemapkernel" \["autolinear"\]|"string tonemapkernel" ["linear"]\
	"float linear_sensitivity" [100.0]\
	"float linear_exposure" [0.002]\
	"float linear_fstop" [16]\
	"float linear_gamma" [2.2]\
	"integer halttime" [240]|' < $v > tmp.lxs
    mv tmp.lxs $v
done

# output settings
for v in *.lxs
do
	sed -E 's|"bool write_resume_flm" \["false"\]|"bool write_resume_flm" ["true"]|' < $v > tmp.lxs
	mv tmp.lxs $v
done
