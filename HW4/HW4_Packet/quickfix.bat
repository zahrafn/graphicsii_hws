REM tonemap settings and halt time
for %%v in (*.lxs) do (
	sed -E "s/\""string\stonemapkernel\""\s\[\""autolinear\""\]/\""string\d032tonemapkernel\""\d032[\""linear\""]\n\""float\d032linear_sensitivity\""\d032[64.0]\n\""float\d032linear_exposure\""\d032[0.002]\n\""float\d032linear_fstop\""\d032[8.0]\n\""float\d032linear_gamma\""\d032[2.2]\n\""integer\d032halttime\""\d032[240]/" < %%v > tmp.lxs
    move tmp.lxs %%v
)

REM output settings
for %%v in (*.lxs) do (
	sed -E "s/\""bool\swrite_resume_flm\""\s\[\""false\""\]/\""bool\d032write_resume_flm\""\d032[\""true\""]/" < %%v > tmp.lxs
	move tmp.lxs %%v
)
