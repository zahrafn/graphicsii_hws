import bpy          # Blender python API
import math         # Basic math
import mathutils    # Special Blender math utilities

# Get the current scene
scn = bpy.context.scene

# Grab the current active camera for the scene and the current frame
cam = scn.camera
curFrame = scn.frame_current

# Find and open the file with the camera information
camFile = open("/Users/Behnaz/Courses/Graphics2/HW4/HW4_Packet/Random100View.vset", "r")

# Read each line in the file
view = 1
for line in camFile:
        
    # Identify only pose lines (start with 'p')
    if line.startswith("p"):
        
        # Break line into individual elements
        elems = line.split()
        X = float(elems[1])
        Y = float(elems[2])
        Z = float(elems[3])
        
        i  = float(elems[4])
        j  = float(elems[5])
        k  = float(elems[6])
        Qr = float(elems[7])

        # Negate the W component of the quaternion then rotate 90 deg around X axis        
        quat = mathutils.Quaternion((-Qr, i, j, k))
        quat.rotate(mathutils.Quaternion((1.0, 0.0, 0.0), math.radians(90.0)))
        
        # Rotate location 90 deg around X axis (just set Y = -Z and Z = Y)
        newLoc = (X, -Z, Y)
        newRot = quat
        
        # Delete the current keyframe
        try:     
            cam.keyframe_delete(data_path="location", frame=view)
            cam.keyframe_delete(data_path="rotation_quaternion", frame=view)
        except RuntimeError:
            # skip exception because the first time this script is run when there are
            # no keyframes, the deletes will fail
            pass

        # Set the camera's location and orientation
        cam.location = newLoc
        cam.rotation_mode = 'QUATERNION'
        cam.rotation_quaternion = newRot

        # Create a new keyframe
        cam.keyframe_insert(data_path="location", frame=view)
        cam.keyframe_insert(data_path="rotation_quaternion", frame=view)
        
        # Move counter forward
        view = view + 1

# All done, close the file and return to frame 1
camFile.close()

scn.frame_start = 1
scn.frame_end = view-1
scn.frame_set(curFrame)
