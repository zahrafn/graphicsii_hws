#! /usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt

data=open('k_FPS.txt','r')
k = []
fps = []


for line in data:
  line = line.split(',')
  k.append(np.int(line[0]))
  fps.append(np.int(line[1]))




plt.plot(k,fps,marker='o',linestyle='--', color='b', label='random')

plt.xlabel('k-samples')
plt.ylabel('frame/sec')
#plt.legend(loc='upper right',bbox_to_anchor=[1,1],ncol=1,shadow=True,fancybox=True)
plt.xlim(1, 11)
plt.ylim(4, 14)
plt.show()

